#!/bin/sh
command -v vim >/dev/null 2>&1 || { echo "Vim not installed."; exit 1; }
command -v zsh >/dev/null 2>&1 || { echo "Zsh not installed."; exit 1; }
command -v tmux >/dev/null 2>&1 || { echo "Tmux not installed."; exit 1; }
command -v git >/dev/null 2>&1 || { echo "Git not installed."; exit 1; }
command -v wget >/dev/null 2>&1 || { echo "Wget not installed."; exit 1; }

mkdir -p ~/.vim/pack/ConsoleConf/start/
CURDIR=`pwd`
cd ~/.vim/pack/ConsoleConf/start/
git clone https://github.com/luochen1990/rainbow.git
git clone https://github.com/derekwyatt/vim-scala.git
git clone https://github.com/w0ng/vim-hybrid.git
git clone https://github.com/plasticboy/vim-markdown.git
git clone https://github.com/godlygeek/tabular.git
cd "$CURDIR"
cp vimrc ~/.vimrc
mkdir -p ~/.vim/tags
ctags -R --sort=1 --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ -f ~/.vim/tags/cpp /usr/lib/gcc/*/*/include

cp zshrc ~/.zshrc
cp zshenv ~/.zshenv
git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.zsh/zsh-autosuggestions/
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.zsh/zsh-syntax-highlighting/

cp tmux.conf ~/.tmux.conf
wget -O tmuxcolors-256.conf https://raw.githubusercontent.com/seebi/tmux-colors-solarized/master/tmuxcolors-256.conf
cat tmuxcolors-256.conf >> ~/.tmux.conf
rm tmuxcolors-256.conf


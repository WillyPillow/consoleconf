This is a set of configurations for *nix command line tools that I personally use, with an automated installer.

### Supported Programs ###

* Zsh
* Vim
* Tmux
* St (Not automatically installed)

### Installation ###

Simply run
```
#!bash
git clone https://bitbucket.org/WillyPillow/ConsoleConf.git
cd ConsoleConf/
./install.sh
```

### St ###

An config file for st is provided at st-config.h. However, this is not automatically installed by install.sh since it needs to be built from source.

Also note that this config is best used on a [Solarized patched instance of st](http://st.suckless.org/patches/solarized).

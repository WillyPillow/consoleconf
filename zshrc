# vim:foldmethod=marker
# Start X and tmux if necessary {{{
if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
  exec startx
fi
if [ -z "$TMUX" ]; then
  export TERM=xterm-256color
  exec tmux
fi
# }}}
# Enviroment variables {{{
EDITOR="vim"
KEYTIMEOUT=1
# }}}
# Options {{{
setopt autocd
setopt beep
setopt extendedglob noglobdots
setopt notify
setopt interactivecomments
setopt correct
# Auto completion settings {{{
zstyle ':completion:*' completer _list _expand _complete _ignored _match _approximate _prefix
zstyle ':completion:*' completions 1
zstyle ':completion:*' glob 1
zstyle ':completion:*' max-errors 3 numeric
zstyle ':completion:*' prompt '<Corrections: %e>'
zstyle ':completion:*' substitute 1
zstyle ':completion:*' menu select
zstyle ':completion:*' list-colors ''
zstyle ':completion::complete:*' use-cache 1
zstyle :compinstall filename '/home/user/.zshrc'
setopt COMPLETE_ALIASES
autoload -Uz compinit
compinit
# }}}
# Command history {{{
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory sharehistory
# }}}
# Prompt {{{
# Prompt: <[Return Code]/[Background Jobs]>[Working Dir] $
autoload -Uz colors && colors
PROMPT="%{$fg[yellow]%}<%?/%j>%~$ %f"
# Display [Normal] on the right if in normal mode
function zle-line-init zle-keymap-select {
    VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]%  %{$reset_color%}"
    RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/} $EPS1"
    zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select
# }}}
# }}}
# Keybinds {{{
bindkey -v
bindkey "^[[1~" beginning-of-line # Home
bindkey "^[[4~" end-of-line # End
bindkey "\e[3~" delete-char # Delete
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^x^x' edit-command-line
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey -a 'k' history-beginning-search-backward
bindkey -a 'j' history-beginning-search-forward
# Send fg on <C-z> {{{
ctrl-z-fg() fg
zle -N ctrl-z-fg
bindkey '^z' ctrl-z-fg
# }}}
# }}}
# Misc {{{
stty -ixon # Disable <C-s> freeze
# }}}
# Plugins {{{
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
# Autosuggest
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE=fg=5
# }}}


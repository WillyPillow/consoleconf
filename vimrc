" vim:foldmethod=marker
" Import sane defaults {{{
unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim
" }}}
" Options {{{
set autochdir
set autoindent cindent smartindent
set autoread
set clipboard^=unnamedplus,unnamed
set cursorline cursorcolumn colorcolumn=80
set encoding=utf8
set fileformats=unix,dos,mac
set foldenable foldmethod=indent foldlevel=5
set gdefault hlsearch
set laststatus=0
set lazyredraw
set list listchars=tab:>·,trail:~,space:·
set nostartofline
set number relativenumber
set rulerformat=%100(%F%m%r%h%w\ %{getcwd()}\ %l,%c\ %p%%%)
set shortmess=aI
set smartcase ignorecase
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab shiftround
set undofile undodir^=$HOME/.vim/undo directory^=$HOME/.vim/swap//
set viewoptions=folds,options,cursor,unix
set virtualedit=onemore,block
set wildmode=longest,list,full
set hidden
" }}}
" Themes {{{
colorscheme hybrid
set background=dark
set termguicolors
highlight TabLine cterm=bold term=bold ctermbg=NONE guibg=NONE
highlight TabLineFill cterm=bold term=bold ctermbg=NONE guibg=NONE
highlight TabLineSel ctermbg=LightGray guibg=#707880
let &t_SI .= "\<Esc>[6 q"
let &t_EI .= "\<Esc>[2 q"
" }}}
" Mappings and abbreviations {{{
" Change <leader> to <Space> {{{
let mapleader="\<Space>"
" }}}
" Save file with sudo {{{
command! W write !sudo tee % > /dev/null
" }}}
" Search with current selection {{{
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-r>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-r>=@/<CR><CR>
function! VisualSelection(direction, extra_filter) range
  let l:saved_reg = @"
  execute "normal! vgvy"
  let l:pattern = escape(@", '\\/.*$^~[]')
  let l:pattern = substitute(l:pattern, "\n$", "", "")
  if a:direction == 'gv'
    call CmdLine("Ag \"" . l:pattern . "\" " )
  elseif a:direction == 'replace'
    call CmdLine("%s" . '/'. l:pattern . '/')
  endif
  let @/ = l:pattern
  let @" = l:saved_reg
endfunction
" }}}
" Swap ; with : {{{
nnoremap ; :
nnoremap : ;
vnoremap ; :
vnoremap : ;
" }}}
" Windows switching {{{
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
" }}}
" Switch to last tab {{{
let g:lasttab = 1
nnoremap <Leader>tl :execute "tabn ".g:lasttab<CR>
autocmd TabLeave * let g:lasttab = tabpagenr()
" }}}
" Toggle pasting and spelling checking {{{
noremap <Leader>pp :setlocal paste!<CR>
noremap <Leader>ss :setlocal spell!<CR>
" }}}
" Copy entire file {{{
noremap <C-a> :%yank<CR>
noremap <C-q> <C-a>
" }}}
" Save file {{{
inoremap <C-s> <Esc>:write<CR>a
noremap <C-s> :write<CR>
" }}}
" Edit and reload vimrc {{{
noremap <Leader>ve :vsplit $MYVIMRC<CR>
noremap <Leader>vl :source $MYVIMRC<CR>
" }}}
" Compile commands {{{
augroup compile
  autocmd!
  autocmd filetype c,cpp
    \ setlocal makeprg=g++\ -O2\ -o\ a.out\ -Wall\ --std=c++11\ %
  autocmd filetype c,cpp nnoremap <F4>
    \ :write <Bar>
    \ make <Bar>
    \ execute '!./a.out'<CR>
  autocmd filetype c,cpp nnoremap <F5>
    \ :write <Bar>
    \ make -g3 -O0 <Bar>
    \ execute 'silent !tmux split-window -h "gdb ./a.out"' <Bar>
    \ :redraw <CR>
  autocmd filetype python nnoremap <F4>
    \ :write <Bar>
    \ execute '!python '.shellescape('%') <CR>
  autocmd filetype scala nnoremap <F4>
    \ :write <Bar>
    \ execute '!scala '.shellescape('%') <CR>
  autocmd filetype scala nnoremap <F5>
    \ :execute 'silent !tmux split-window -h "scala"' <CR>
augroup END
" }}}
" UVa tools using https://github.com/lucastan/uva-node {{{
nnoremap <F12> :execute '!uva status'<CR>
command! -nargs=1 Uvav !uva view <q-args>
command! -nargs=1 Uvas execute '!uva send <q-args> %' <Bar> execute '!uva status'
" }}}
" Insert freopen for debugging of competitive programming {{{
abbreviate freo #ifndef ONLINE_JUDGE<CR>freopen("in.txt", "r", stdin);<CR>freopen("out.txt", "w", stdout);<CR>#endif
" }}}
" Accelerate iostream in C++ {{{
abbreviate ioso std::cin.tie(0);<CR>std::ios_base::sync_with_stdio(0);
" }}}
" Arrows used for windows resizing {{{
nnoremap <Left> :vertical resize -2<CR>
nnoremap <Right> :vertical resize +2<CR>
nnoremap <Up> :resize -2<CR>
nnoremap <Down> :resize +2<CR>
" }}}
" More text objects {{{
for char in [ '_', '.', ':', ',', ';', '<bar>', '/', '<bslash>', '*', '+', '%', '`' ]
    execute 'xnoremap i' . char . ' :<C-u>normal! T' . char . 'vt' . char . '<CR>'
    execute 'onoremap i' . char . ' :normal vi' . char . '<CR>'
    execute 'xnoremap a' . char . ' :<C-u>normal! F' . char . 'vf' . char . '<CR>'
    execute 'onoremap a' . char . ' :normal va' . char . '<CR>'
endfor
" }}}
" Dot command in visual mode {{{
vnoremap . :normal.<CR>
" }}}
" Align Markdown tables {{{
augroup markdown_align
  autocmd!
  autocmd filetype md,markdown
    \ vnoremap <leader>al !column -s '\|' -o '\|' -t<CR>
augroup END
" }}}
" }}}
" Misc {{{
" Cursorline on focus {{{
augroup highlight_follows_focus
    autocmd!
    autocmd WinEnter * set cursorline
    autocmd WinLeave * set nocursorline
augroup END
" }}}
" Auto open quickfix window {{{
augroup autoquickfix
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost    l* lwindow
augroup END
" }}}
" }}}
" Plugins {{{
" Enable Rainbow parens
let g:rainbow_active = 1
" Hybrid theme
let g:hybrid_custom_term_colors = 1
" }}}
